import styles from "./Paragraph.module.scss";
import { IPara } from "./Paragraph.types";

const Paragraph =({text}: IPara) => {
    return(
        <>
        {
            text.map((p,index)=> {
                return(
                    <p className={styles.para}key={index}>{p}</p>
                )
            })
        }
        </>
    )
}

export default Paragraph;