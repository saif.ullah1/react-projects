import {ChangeEvent,MouseEvent} from 'react';
export interface Iform{
    count:number,
    onChange: (e:ChangeEvent<HTMLInputElement>)=> void;
    onSubmit:(e:MouseEvent<HTMLButtonElement>)=> void

}