import styles from './Form.module.scss'
import { Iform } from './Form.types';

const Form =({count, onChange,onSubmit}:Iform) => {
    return (
        <form className={styles.loremForm}>
          <label htmlFor="amount">Paragraphs:</label>
          <input
            type="number"
            name="amount"
            id="amount"
            value={count}
            onChange={(e)=> onChange(e)}
          />
          <button type="submit" 
          onClick={(e)=>onSubmit(e)}
          className={styles.btn}>Generate</button>
        </form>
    )
}

export default Form;
