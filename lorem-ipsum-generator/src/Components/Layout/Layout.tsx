import { useState } from "react";
import { fetchData } from "../../Services/getData.service";
import Form from "../Form/Form";
import Paragraph from "../Paragraph/Paragraph";
import styles from "./Layout.module.scss";
import { MouseEvent } from 'react';
import { ChangeEvent } from 'react';


const Layout=()=>{
    const [text, setText] = useState<string[]>();
    const [count, setCount] = useState(0);
  
    const getData = async () => {
      const data = await fetchData();
      if(count > 8) {
        setText(data.slice(0,count));
      }
      if(count <=0){
        setText(data.slice(0,1))
      }
      if(count > 0){
        setText(data.slice(0,count))
      }
    };
  
    const onSubmit = (e: MouseEvent<HTMLButtonElement>) => {
      e.preventDefault();
      getData();
    }
  
    const onChange = (e: ChangeEvent<HTMLInputElement>) => {
      const value = parseInt(e.currentTarget.value);
      setCount(value);
    };
    
    return (
      <main className={styles.main}>
        <div className={styles.container}>
          <h2>Tired of boring lorem ipsum?</h2>
          <Form count={count} onChange={onChange}
          onSubmit = {onSubmit}/>
          {
            text && <Paragraph text={text}/>
          }
        </div>
      </main>
    );
  
}

export default Layout;