export interface IData {
    id:number;
    image:string,
    name:string,
    title:string,
    quote:string
    }
    
    export type Data = IData[];
    