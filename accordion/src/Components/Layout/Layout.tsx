import { useEffect, useState } from "react";
import { Data } from "../../Services/Data.type";
import { getData } from "../../Services/getData.service";
import Question from "../Question/Question";
import styles from "./Layout.module.scss";

const Layout=()=>{
    const [question, setQuestion] = useState<Data>();

  

    const getQuestion = async () => {
      const data = await getData();
      setQuestion(data);
    };
  
    useEffect(() => {
      getQuestion();
    }, []);
  
    return (
      <main className={styles.main}>
        
        <div className={styles.questionContainer}>
        <h1>Question and Answers about Login</h1>
        {
          question && question.map((question,index)=> {
            return (
              <Question question={question} key={question.id} />
            )
          })
        }
        </div>
        
      </main>
    );
  
}

export default Layout;