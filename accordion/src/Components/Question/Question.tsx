import styles from "./Question.module.scss";
import { IQuestion } from "./Question.type";
import AddCircleOutlineRoundedIcon from "@mui/icons-material/AddCircleOutlineRounded";
import RemoveRoundedIcon from "@mui/icons-material/RemoveRounded";
import { useState } from "react";

const Question = ({ question }: IQuestion) => {
  const [showInfo, setShowInfo] = useState(false);
  return (
    <div className={styles.question}>
      <h3>
        {question.title}
        <span className={styles.toggle}>{showInfo ? (
          <RemoveRoundedIcon className={styles.icon} onClick={()=> setShowInfo(!showInfo)} />
        ) : (
          <AddCircleOutlineRoundedIcon className={styles.icon} onClick={()=> setShowInfo(!showInfo)} />
        )}</span>
        
      </h3>
      {showInfo && <p>{question.info}</p>}
    </div>
  );
};

export default Question;
