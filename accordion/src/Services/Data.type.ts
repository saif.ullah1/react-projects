export interface IData {
    id:number;
    title: string;
    info:string;
}

export type Data = IData[];
