import React from 'react';
import {IPeople, IPeoples} from './List.type'

const List = ({ people}:any) => {
  return (
    <>
      {people.map((person:IPeople) => {
        const { id, name, age, image } = person;
        return (
          <article key={id} className='person'>
            <img src={image} alt={name} />
            <div>
              <h4>{name}</h4>
              <p>{age} years</p>
            </div>
          </article>
        );
      })}
    </>
  );
};

export default List;