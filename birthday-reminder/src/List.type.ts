export interface IPeople{
    id:number,
    name:string,
    age:number,
    image:string
}
export type IPeoples=IPeople[]