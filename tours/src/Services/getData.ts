import { Data } from "./Data.type";

export const getData=async():Promise<Data>=>{
    const response= await fetch("https://course-api.com/react-tours-project");
    const data=await response.json() as Data;
    return data;
}