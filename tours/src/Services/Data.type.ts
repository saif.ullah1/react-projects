export interface IData{
    id:string,
    name:string,
    info:string,
    image:string,
    price:string
}
export type Data = IData[];