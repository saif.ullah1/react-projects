import { useEffect, useState } from "react";
import { Data } from "../../Services/Data.type";
import { getData } from "../../Services/getData";
import Loading from "../Loading/Loading";
import TourList from "../TourList/TourList";
import styles from "./Layout.module.scss";

const Layout=()=>{
    const[tour,SetTour]=useState<Data>();
    const[loading,setloading]=useState(true);

    const fetchTour=async()=>{
        const data=await getData();
        SetTour(data);
        setloading(false);
    };
    const deleteTour=(id:string)=>{
        const updatedTours=tour?.filter((tour)=>tour.id!==id);
        SetTour(updatedTours);
    };

    useEffect(()=>{
        fetchTour();
    },[]);

    if(loading){
        return(
            <main className={styles.main}>
                <Loading/>
            </main>
        )
    }
    if(tour?.length===0){
        return(
            <main className={styles.main}>
                <div>
                    <h1>No tour left</h1>
                </div>
                <button className={styles.refresh} onClick={fetchTour}>Refresh</button>
            </main>
        )
    }
    return(
        <main className={styles.main}>
            {tour && <TourList data={tour} onClick={deleteTour}/>}
        </main>
    )
}
export default Layout;