export interface Itourcard{
    id:string,
    name:string,
    info:string,
    url:string,
    price:string,
    onClick:(t:string)=>void
}