import { useState } from "react";
import { Itourcard } from "./TourCard.type";
import styles from "./TourCard.module.scss"
const TourCard = ({ id, name, info, url, price, onClick }: Itourcard) => {
    const [readMore, setreadMore] = useState(false);
    return (
        <div className={styles.tourCardContainer}>

            <img src={url} alt={name} />
            <div className={styles.tourInfo}>
                <h4>{name}</h4>
                <h4 className={styles.tourPrice}>{price}</h4></div>
            <div className={styles.tourInfoPart}>
                <p>{readMore ? info : `${info.substring(0, 200)}`}
                    <button onClick={() => setreadMore(!readMore)}>
                        {readMore ? "show less" : "show more"}
                    </button>
                </p>
                <button className={styles.removeBtn} onClick={() => { onClick(id) }}>Not Intrested</button>
            </div>


        </div>
    )
}
export default TourCard;