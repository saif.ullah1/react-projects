import TourCard from "../TourCard/TourCard";
import { Itour } from "./Tour.type";
import styles from "./TourList.module.scss"

const TourList = ({ data, onClick }: Itour) => {
    return (
        <div className={styles.ListContainer}>
            <div className={styles.title}>
                <h1>Our Tours</h1>
            </div>
            {
                data.map((t) => {
                    const {id} = t
                    return (
                        <TourCard key={id} id={t.id} name={t.name} info={t.info} url={t.image} price={t.price} onClick={onClick} />
                    )
                })
            }
        </div>
    )
}

export default TourList;