import { useEffect, useState } from "react";
import { Data } from "../../Services/Data.types";
import { getData } from "../../Services/getData";
import Categories from "../Categories/Categories";
import { CategoryType } from "../Categories/Category.type";
import Menu from "../Menu/Menu";
import styles from "./Layout.module.scss"

const Layout=()=>{
    const [menu, setMenu] = useState<Data>();
    const [categories, setCategories] = useState<CategoryType[]>();
  
    const getMenu = async () => {
      const data = await getData();
      setMenu(data);
      abc(data)
      return data;
    };
  
    useEffect(() => {
      getMenu();
    }, []);
  
    const filterItems = async (category: CategoryType) => {
      const data = await getData();
       
        if (category === "all") {
          getMenu();
        }
        const newMenu = data.filter((item) => item.category === category);
        setMenu(newMenu);
      
    };
   
    const abc = (data: Data) => {
      const categories = new Set<CategoryType>(data.map((item) => item.category));
      const allCategories: CategoryType[] = ["all", ...Array.from(categories)];
      setCategories(allCategories);
    };
  
    return (
      <div className={styles.main}>
        <div className={styles.menuContainer}>
          <h1>Our Menu</h1>
  
          {categories && (
            <Categories categories={categories} filterItems={filterItems} />
          )}
          {menu && <Menu menu={menu} />}
        </div>
      </div>
    );
  
}
export default Layout;