import { IData } from "../../Services/Data.types";
import styles from "./Menu.module.scss";
import { IMenu } from "./Menu.type";

const Menu = ({ menu }: IMenu) => {
  return (
    <div className={styles.sectionCenter}>
      {menu.map((item: IData) => {
        const { id,img, price, desc, title } = item;
        return (
          <div key={id} className={styles.menu}>
            <img className={styles.img} src={img} alt={title}  />
            <div className={styles.title}>
              <h2>{title}</h2>
              <h4>${price}</h4>
            </div>
            <div className={styles.info}>
              <p>{desc}</p>
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default Menu;
