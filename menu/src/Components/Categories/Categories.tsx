import styles from './Categories.module.scss'
import { ICategory } from './Category.type';

const Categories = ({categories, filterItems}:ICategory) => {
    return (
    <div className={styles.categories}>
        {
            categories.map((category,index)=> {
                return (
                        <button key={index} onClick={()=> filterItems(category)}>{category}</button>
                )
            })
        }
    </div>
    )
}

export default Categories;
