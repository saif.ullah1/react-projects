export type CategoryType = 'all'|'breakfast'|'lunch'|'shakes'
export interface ICategory {
    categories: CategoryType[];
    filterItems: (category: CategoryType)=> void
}
