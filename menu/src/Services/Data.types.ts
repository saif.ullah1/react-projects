export interface IData {
    id: number;
    title: string;
    category: "breakfast" | "lunch" | "shakes";
    price: number;
    img: string;
    desc: string;
  }
  
  export type Data = IData[];
  