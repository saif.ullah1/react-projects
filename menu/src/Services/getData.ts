import { Data } from "./Data.types";
import { fetch } from "./mock.backend";

export const getData = async (): Promise<Data> => {
  const response = (await fetch("URL")) as Data;
  return response;
};
