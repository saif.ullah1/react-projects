import { Data } from "./data.type";

export const getData = async(): Promise<Data> => {
    const response = await fetch('https://course-api.com/react-tabs-project')
    const data =await response.json();
    return data;
}
