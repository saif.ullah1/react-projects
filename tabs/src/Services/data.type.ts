export interface IData {
    id:string;
    order: number;
    title: string;
    dates:string
    duties: string[];
    company: string; 
    }
    
    export type Data = IData[];
    