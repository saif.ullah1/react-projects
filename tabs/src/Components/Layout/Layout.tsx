import { useEffect, useState } from "react";
import { Data } from "../../Services/data.type";
import { getData } from "../../Services/getData.service";
import DataCard from "../DataCard/DataCard";
import styles from"./Layout.module.scss"

const Layout=()=>{

    const[jobs,setJobs]=useState<Data>();

    const fethJobs=async()=>{
        const data=await getData();
        setJobs(data);
    }

    useEffect(()=>{
        fethJobs()
    },[]);

    return( jobs &&
        <DataCard
        jobs={jobs}
        />
         )
}
export default Layout;