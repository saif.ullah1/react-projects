import { useEffect, useState } from "react";
import getData from "../../Services/Review.service";
import { Data } from "../../Services/Data.type";
import Review from "../Review/Review";
import styles from "./Layout.module.scss"

const Layout=()=>{

    const[reviews,setReviews]=useState<Data>();
    const[index,setIndex]=useState(0);
    const getReview=async()=>{
        const data=await getData();
        setReviews(data);
    }

    useEffect(()=>{
        getReview()
    },[]);

    const prevReview=(index:number)=>{
        if(reviews){
            if(index>0){
                setIndex(index-1);
            }
            if(index===0){
                setIndex(reviews.length-1);
            }
        }
    };
    const nextReview=(index:number)=>{
        if (reviews) {
            if (index+1 < reviews.length) {
              setIndex(index + 1);
            }
      
            if(index+1 === reviews.length){
              setIndex(0)
            }
          }      
    };
    const randomPerson = ( index: number ) => {
        if(reviews) {
          
          const randomIndex = Math.floor(Math.random()*reviews.length)
          if(randomIndex !== index){
            setIndex(randomIndex);
          }
          if(randomIndex === index){
            if (randomIndex > 0) {
              setIndex(randomIndex - 1);
            }
            if (randomIndex+1 < reviews.length) {
              setIndex(randomIndex + 1);
            }
          }
          
          
        }
      }
    
    return(<main className={styles.main}>
        <div className={styles.reviewContainer}>
          <h1>Our Reviews</h1>
          {reviews && <Review nextPerson={nextReview} prevPerson={prevReview} review={reviews[index]} index = {index} randomPerson={randomPerson}/>}
        </div>
      </main>
  

        )
}

export default Layout;