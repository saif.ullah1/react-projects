import { IData } from "../../Services/Data.type";

export interface IReview {
  review: IData;
  index: number;
  nextPerson: (index: number) => void;
  prevPerson: (index: number) => void;
  randomPerson: (index: number) => void;
}
