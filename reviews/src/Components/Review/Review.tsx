import styles from "./Review.module.scss";

import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";

import ChevronRightIcon from "@mui/icons-material/ChevronRight";

import FormatQuoteIcon from "@mui/icons-material/FormatQuote";

import { IReview } from "./Review.type";

const Review = ({ review,nextPerson,prevPerson,index, randomPerson }: IReview) => {
  const { name, job, image, text } = review;
  return (
    <div className={styles.review}>
      <div className={styles.imageContainer}>
        <img src={image} alt={name} />
        <span >
            <FormatQuoteIcon className={styles.quoteIcon}/>
        </span>
      </div>
      <div className={styles.info}>
          <h4>{name}</h4>
          <p className={styles.job}>{job}</p>
          <p>{text}</p>
      </div>
      <div className={styles.navigate}>
      <ChevronLeftIcon onClick={()=> prevPerson(index)} className={styles.leftBtn}/>
      <ChevronRightIcon onClick={()=> nextPerson(index)} className={styles.rightBtn}/>
      </div>
      <button onClick = {()=> randomPerson(index)} className={styles.randomBtn}>Suprise Me</button>
      
    </div>
  );
};

export default Review;
