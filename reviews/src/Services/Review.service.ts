import { fetch } from "./Backend.mock";
import { Data } from "./Data.type";

const getData=async():Promise<Data>=>{
    const data=await fetch("DUMMY-URL");
    return data;
}
export default getData;