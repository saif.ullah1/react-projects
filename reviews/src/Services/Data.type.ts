export interface IData{
   
        id: number;
        name:string;
        job:string;
        image: string;
        text: string;
}
export type Data = IData[];
